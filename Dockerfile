FROM node

LABEL MAINTAINER="Nicolás Pons" \
      EMAIL="nikkopons@gmail.com"

#ENV NPM_CONFIG_LOGLEVEL info
EXPOSE 8080

WORKDIR /usr/app
RUN npm install http-server -g

ENTRYPOINT ["http-server"]
CMD ["--help"]


#docker run -it --rm http_server_container
#cd web
#docker run -it -v ${pwd}:/app -p 8080:8080 --rm http_server_container /app 8080